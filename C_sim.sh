for file in test_progs/*.c; 
do
	#file=$(echo $file | cut -d'.' -f1)
	echo "Cleaning Directory"
	make clean
	echo "Compiling $file"
	make program SOURCE=$file
	echo "Running $file"
	make all
	echo "Saving $file output"
	file=$(echo $file | cut -d'.' -f1)
	program=$(echo "${file}.program.out")
	mv program.out $program
	writeback=$(echo "${file}.writeback.out")
	mv writeback.out $writeback
done

mv test_progs/*.program.out generated_results/
mv test_progs/*.writeback.out generated_results/
