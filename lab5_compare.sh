for file in generated_results/*; 
do 
	flag=1
	file=$(echo $file | cut -d'/' -f2)
	if [[ "$file" == *"writeback"* ]]; then
		if diff generated_results/${file} ../../lab5/verisimplev/ground_truth/${file} >/dev/null; then
			flag=$flag
		else
			flag=0
		fi
	fi
	if [[ "$file" == *"program"* ]]; then
		grep @@@ generated_results/${file} > A.txt
		grep @@@ ../../lab5/verisimplev/ground_truth/${file} > B.txt
		if diff A.txt B.txt >/dev/null; then
			flag=$flag
		else
			flag=0
		fi
		rm *.txt
	fi
	if (( $flag == 1 )); then
		echo -e "\e[32mPassed for ${file}"
	else
		echo -e "\e[31mFailed for ${file}"
	fi
done
